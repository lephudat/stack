#pragma once
#ifndef _Queue_H
#define _Queue_H
#include<iostream>
using namespace std;

template<class T>
struct QueueNode
{
	T data;
	QueueNode*pNext;
};

template<class T> class Queue
{
private:
	QueueNode<T>*Front;
	QueueNode<T>*Rear;
	unsigned short size;
	QueueNode<T>*createQueueNode(T data);
public:
	Queue();
	~Queue();
	void display();
	Queue<T>* split(int n);
	bool enQueue(T data);
	bool deQueue(T &data);
	int Size();
};

#include"Queue.cpp"
#endif // !_Queue_H
