#ifdef _Binary_Search_Tree_H


#include"Binary_Search_Tree.h"

template<class T>
void Binary_Search_Tree<T>::print_Preorder(BST_Node<T>* B)
{
	if (B == NULL)
		return;
	out << B->data << " ";
	cout << B->data << " ";	//Visit root BST_Node
	print_Preorder(B->left); //Traversal the left subtree
	print_Preorder(B->right); //Traversal the right subtree
}

template<class T>
void Binary_Search_Tree<T>::print_Inorder(BST_Node<T>* B)
{
	if (B == NULL)
		return;
	print_Inorder(B->left);//Traversal the left subtree
	cout << B->data << " ";//Visit root BST_Node
	out << B->data << " ";
	print_Inorder(B->right);//Traversal the right subtree
}

template<class T>
void Binary_Search_Tree<T>::print_Postorder(BST_Node<T>* B)
{
	if (B == NULL)
		return;
	print_Postorder(B->left);//Traversal the left subtree
	print_Postorder(B->right);//Traversal the right subtree
	cout << B->data << " ";//Visit root BST_Node
	out << B->data << " ";
}

template<class T>
void Binary_Search_Tree<T>::find(BST_Node<T>* B, int x)
{
	if (B == NULL)
	{
		cout << "NULL\n";
		return;
	}
	if (B->data == x)
	{
		cout << x << endl;
		return;
	}
	else
	{
		if (x < B->data)
		{
			cout << "left ";
			find(B->left, x);
		}
		else
		{
			cout << "right ";
			find(B->right, x);
		}
	}
}

template<class T>
void Binary_Search_Tree<T>::printLevelOrder(BST_Node<T>* B, int Level)
{
	if (B == NULL)
		return;
	if (Level == 1)
	{
		cout << B->data << " ";
		out << B->data << " ";
	}

	else
	{
		printLevelOrder(B->left, Level - 1);//Traversal the left subtree with Level - 1
		printLevelOrder(B->right, Level - 1);//Traversal the right subtree with Level - 1
	}
}

template<class T>
int Binary_Search_Tree<T>::height(BST_Node<T>* B)
{
	if (B == NULL)
		return 0;
	if (B->left == NULL && B->right == NULL)
		return 1;
	return(1 + height(B->left) + height(B->right));
}

template<class T>
void Binary_Search_Tree<T>::print_Level(BST_Node<T>* B)
{
	for (int i = 1; i < height(B) + 1; i++)
		printLevelOrder(B, i);
}

template<class T>
void Binary_Search_Tree<T>::re_convert_Queue(BST_Node<T>* B, Queue<T>& Q, int Level)
{
	if (B == NULL)
		return;
	if (Level == 1)
		Q.enQueue(B->data);
	else
	{
		re_convert_Queue(B->left, Q, Level - 1);
		re_convert_Queue(B->right, Q, Level - 1);
	}

}

template<class T>
BST_Node<T>* Binary_Search_Tree<T>::cpy(BST_Node<T>* B, Queue<int>& Q)
{
	for (int i = 1; i <= height(B); i++)
		re_convert_Queue(B, Q, i);
	BST_Node<T>*B1 = NULL;
	int get;
	for (int i = 0; i < Q.Size(); i++)
	{
		Q.deQueue(get);
		Add(B1, get);
	}
	return B1;
}

template<class T>
Binary_Search_Tree<T>::Binary_Search_Tree()
{
	root = NULL;
	out.open("log.txt", ios::app);
	out << "Binary Search Tree:\n--------Constructor:\t root=NULL\n";
}

template<class T>
Binary_Search_Tree<T>::~Binary_Search_Tree()
{

	out << "----------Destructor:\n";
	while (root != NULL)
	{
		out << "Remove: " << root->data << endl;
		remove(root->data);
	}
	out << "\n\n\n";
	out.close();
}

template<class T>
Binary_Search_Tree<T>::Binary_Search_Tree(const Binary_Search_Tree & Destination)
{
	Queue<T> Q;
	BST_Node<T>* tmp = cpy(root, Q);
}

template<class T>
bool Binary_Search_Tree<T>::add(T data)
{

	return Add(root, data);
}

template<class T>
bool Binary_Search_Tree<T>::remove(T data)
{
	return Remove(root, data);
}

template<class T>
void Binary_Search_Tree<T>::display_Postorder()
{
	cout << "Traversal Postorder:\t";
	out << "Traversal Postorder:\t";
	print_Postorder(this->root);
	cout << endl; 
	out << endl;
}

template<class T>
void Binary_Search_Tree<T>::display_Inorder()
{
	cout << "Traversal Inorder:\t";
	out << "Traversal Inorder:\t";
	print_Inorder(root);
	cout << endl;
	out << endl;
}

template<class T>
void Binary_Search_Tree<T>::display_Preorder()
{
	cout << "Traversal Preorder:\t";
	out << "Traversal Preorder:\t";
	print_Preorder(root);
	cout << endl;
	out << endl;
}

template<class T>
void Binary_Search_Tree<T>::display_Level()
{
	cout << "Traversal level:\t";
	out << "Traversal level:\t";
	print_Level(root);
	cout << endl;
	out << endl;
}

template<class T>
inline BST_Node<T>* Binary_Search_Tree<T>::create_BST_Node(T data)
{
	out << "Create node: " << data << endl;
	BST_Node<T>*p = new BST_Node<T>;//Allocate memory for new BST_Node

	p->data = data;//Assign data to this BST_Node

	p->left = NULL;//Inlitialize left and right childrend as NULL 
	p->right = NULL;

	return p;
}

template<class T>
inline bool Binary_Search_Tree<T>::Add(BST_Node<T>*& B, T data)
{
	out << "Add node " << data << endl;
	BST_Node<T>*p = create_BST_Node(data);
	if (p == NULL)
	{
		out << "Unallocate new node for data \n";
		return false;	//unallocated new BST_Node;
	}if (B == NULL)
	{
		out << "root is NULL so insert now\n";
		B = p;
	}
	else
	{
		if (data < B->data)
		{
			out << "New data is smaller than root's data\n";
			Add(B->left, data);
		}

		else
		{
			if (data > B->data)
			{
				out << "New data is greater than root's data\n";
				Add(B->right, data);
			}

			else
			{
				out << "New data  is present at root's data\n";
				return false;	//existed
			}
		}
	}
	return false;
}

template<class T>
inline T Binary_Search_Tree<T>::max_value(BST_Node<T>* B)
{
	while (B->right != NULL)
		B = B->right;
	return B->data;
}

template<class T>
inline T Binary_Search_Tree<T>::min_value(BST_Node<T>* B)
{
	while (B->left != NULL)
		B = B->left;
	return B->data;
}
template<class T>
bool Binary_Search_Tree<T>::Remove(BST_Node<T>*& B, T data)
{
	out << "Remove: " << data << endl;
	if (B == NULL)
	{
		out << "no existed node " << data << " or this's the end of tree\n";
		return false;	//nothing or the end
	}
	if (data == B->data)
	{
		out << "data = root's data\n";
		if (B->left != NULL &&B->right != NULL)
		{
			out << "root's left and right childs is differ NULL\n";
			T max = max_value(B->left);
			B->data = max;
			out << "Find max = " << max << endl;
			Remove(B->left, max);
		}
		else
		{
			if (B->left == NULL)
			{
				out << "root's left child is NULL\n";
				BST_Node<T>*tmp = B;
				B = B->right;
				delete tmp;
			}
			else
			{
				if (B->right == NULL)
				{
					out << "root's right child is NULL\n";
					BST_Node<T>*tmp = B;
					B = B->left;
					delete tmp;
				}
				else
				{

					out << "Both root's right and left childs are NULL\n";
					delete B;
					B = NULL;
				}
			}
		}
	}
	else
	{
		if (data < B->data)
		{
			out << data << " is smaller than " << B->data << " so remove left subtree w/ data = " << data << endl;
			Remove(B->left, data);
		}
		else
		{
			out << data << "is smaller than " << B->data << " so remove left subtree w / data = " << data << endl;
			Remove(B->right, data);
		}
	}
	return true;
}
#endif