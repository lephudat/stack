#ifdef _Queue_H
template<class T>
QueueNode<T>* Queue<T>::createQueueNode(T data)
{
	QueueNode<T>*p = new QueueNode<T>;
	p->data = data;
	p->pNext = NULL;
	return p;
}

template<class T>
Queue<T>::Queue()
{
	Front = NULL;
	Rear = NULL;
	size = 0;
}

template<class T>
Queue<T>::~Queue()
{
	int x;
	while (Front != NULL)
	{
		deQueue(x);
	}
}

template<class T>
void Queue<T>::display()
{
	QueueNode<T>*cur = Front;
	while (cur != NULL)
	{
		cout << cur->data << " ";
		cur = cur->pNext;
	}
	cout << endl;
}

template<class T>
Queue<T>* Queue<T>::split(int n)
{
	int j = 0;
	Queue<T>*Q = new Queue<T>[n];
	QueueNode<T>*cur = Front;
	while (cur != NULL)
	{
		for (j = 0; j < n; j++)
		{
			Q[j].enQueue(cur->data);
			cur = cur->pNext;
			if (cur == NULL)
				break;
		}
	}
	return Q;
}

template<class T>
bool Queue<T>::enQueue(T data)
{
	QueueNode<T>*p = createQueueNode(data);
	if (p == NULL)
		return false;
	size++;
	if (Front == NULL) Front = p;
	else Rear->pNext = p;
	Rear=p;
	return true;
}

template<class T>
bool Queue<T>::deQueue(T & data)
{
	if (Front == NULL)
		return false;
	data =Front->data;
	QueueNode<T>*tmp = Front;
	Front = Front->pNext;
	delete tmp;
}

template<class T>
int Queue<T>::Size()
{
	return size;
}
#endif // DEBUG