#pragma once
#ifndef _Binary_Search_Tree_H
#define _Binary_Search_Tree_H


#include<iostream>
#include<fstream>
#include"Queue.h"
using namespace std;

template<class T>
struct BST_Node
{
	T data;
	BST_Node* left;
	BST_Node* right;
};

template <class T>
class Binary_Search_Tree
{
private:
	BST_Node<T>* root;
	ofstream out;
	unsigned short size;

	BST_Node<T> * create_BST_Node(T data);
	bool Add(BST_Node<T> *& B, T data);
	T max_value(BST_Node<T>*B);
	T min_value(BST_Node<T>*B);
	bool Remove(BST_Node<T> *& B, T data);
	void print_Preorder(BST_Node<T> * B);
	void print_Inorder(BST_Node<T> * B);
	void print_Postorder(BST_Node<T> * B);
	void find(BST_Node<T> * B, int x);
	void printLevelOrder(BST_Node<T>*B, int Level);
	int height(BST_Node<T>*B);
	void print_Level(BST_Node<T>*B);
	void re_convert_Queue(BST_Node<T>*B, Queue<T> &Q, int Level);
	BST_Node<T>* cpy(BST_Node<T>* B, Queue<int> &Q);

public:
	Binary_Search_Tree();
	~Binary_Search_Tree();
	Binary_Search_Tree(const Binary_Search_Tree& Destination);

	bool add(T data);
	bool remove(T data);
	void display_Postorder();
	void display_Inorder();
	void display_Preorder();
	void display_Level();
};

#include"Binary_Search_Tree.cpp"
#endif // !Binary_Search_Tree

